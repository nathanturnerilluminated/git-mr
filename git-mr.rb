class GitMr < Formula
  desc "Checkout GitLab merge requests / GitHub pull requests locally"
  homepage "https://gitlab.com/glensc/git-mr"
  url "https://gitlab.com/glensc/git-mr/-/archive/1.0.0/git-mr-1.0.0.tar.bz2"
  sha256 "d596acf623ebdb55a38d2e6d1d1b1cbad0d822323f4c6a5c22f84b986988a5b9"
  head "https://gitlab.com/glensc/git-mr"

  def install
    bin.install Dir["git-*"]
  end

  conflicts_with "git-extras", :because => "both install a `git-pr` and `git-mr` script"
end

# vim:ts=2:sw=2:et
